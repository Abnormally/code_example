<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker $faker) {

    return [
        'first_name'     => $faker->firstName,
        'surname'        => $faker->lastName,
        'nickname'       => $faker->userName,
        'email'          => $faker->unique()->safeEmail,
        'password'       => \Hash::make('password'),
    ];

});
