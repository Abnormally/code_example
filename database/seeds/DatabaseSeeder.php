<?php

use App\Abnormally\Seed\Auth\ClientSeeder;
use App\Abnormally\Seed\Auth\PermissionSeeder;
use App\Abnormally\Seed\Auth\RoleSeeder;
use App\Abnormally\Seed\Auth\UserSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->call(ClientSeeder::class)
            ->call(RoleSeeder::class)
            ->call(PermissionSeeder::class)
            ->call(UserSeeder::class);
    }

}
