const base_url = "/api/v1/";

const App = {
    namespaced: true,
    state: {
        app_options: {
            uri: {
                user: {
                    login: `${base_url}user/auth/sign-in`,
                    register: `${base_url}user/auth/sign-up`,
                    password: `${base_url}user/crud/password`,
                    crud: `${base_url}user/crud`,

                    // Utils section
                    avatar: `${base_url}user/utils/avatar`,
                },
                post: {
                    index: `${base_url}post`,
                    crud: `${base_url}post/crud`,

                    activate: (post_id) => {
                        return `${base_url}post/utils/activate/${post_id}`;
                    },
                    commentabble: (post_id) => {
                        return `${base_url}post/utils/commentable/${post_id}`;
                    },
                }
            }
        },
    },
    mutations: {},
    getters: {
        options: state => {
            return state.app_options;
        },
        getTest: () => {
            return "Test successful";
        },
    },
};

export default App;
