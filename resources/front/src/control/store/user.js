const User = {
    namespaced: true,
    state: {
        user: {
            id: 0,
            is_admin: false,
            is_super: false,
        },
        token_data: {
            token_type: "Some string, default - bearer",
            access_token: null
        },
        success: false,
    },
    mutations: {
        setUser: (state, payload) => {
            localStorage.setItem('user-data', JSON.stringify(payload));

            state.token_data = payload.auth;
            state.success = payload.success;
            state.user = payload.user;
            if (state.user.avatar && state.user.avatar.url) {
                /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\.(jpe?g|png|svg|bmp)$/.test(state.user.avatar.url)
                || (state.user.avatar = null);
            }
        },
        clearUser: state => {
            state.success = false;

            state.user = {
                id: 0,
                is_admin: false,
                is_super: false,
            };

            state.token_data = {
                token_type: "Some string, default - bearer",
                access_token: "Token",
            }
        },
        updateUser: (state, payload) => {
            state.user = payload.user;

            let data = JSON.parse(localStorage.getItem('user-data'));
            data.user = payload.user;
            localStorage.setItem('user-data', JSON.stringify(data));
        }
    },
    getters: {
        user: state => state.user,
        token: state => state.token_data,
        success: state => state.success,
    },
};

export default User;
