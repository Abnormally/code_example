import Vue from 'vue';
import axios from 'axios';

const http = axios;
Vue.prototype.$http = http;

http.interceptors.request.use(request => {
    request.headers.common['Accept'] = "application\/json";
    let token;

    if ((token = localStorage.getItem('user-data'))) {
        token = JSON.parse(token).auth;
        request.headers.common['Authorization'] = `${token.token_type} ${token.access_token}`;
    }

    return request
});

export default http;
