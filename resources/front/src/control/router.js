import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/views/Home'),
        },
        {
            path: '/post',
            component: () => import('@/views/layouts/Empty.vue'),
            children: [
                {
                    path: 'board',
                    name: 'post.main',
                    component: () => import('@/views/post/PostDashboard.vue'),
                },
                {
                    path: 'new',
                    name: 'post.add',
                    component: () => import('@/views/post/PostManagement.vue'),
                },
                {
                    path: 'view/:post_id',
                    name: 'post.view',
                    component: () => import('@/views/post/PostView.vue'),
                },
                {
                    path: 'edit/:post_id',
                    name: 'post.edit',
                    component: () => import('@/views/post/PostManagement.vue'),
                },
            ]
        },
        {
            path: "/user",
            component: () => import('@/views/layouts/Empty.vue'),
            children: [
                {
                    path: '/',
                    name: 'user.profile',
                    component: () => import('@/views/user/Profile.vue'),
                },
            ]
        },
        // {
        //     path: '/dashboard',
        //     component: () => import('@/views/layouts/Master'),
        //     children: [
        //         {
        //             path: 'chat',
        //             component: () => import('@/views/layouts/Empty.vue'),
        //             children: [
        //                 {
        //                     path: '/',
        //                     name: 'chat.board',
        //                     component: () => import('@/views/chat/ChatDashboard.vue')
        //                 },
        //                 {
        //                     path: ':chat_id',
        //                     name: 'chat.pick',
        //                     component: () => import('@/views/chat/ChatCurrent.vue')
        //                 },
        //             ],
        //         },
        //     ]
        // },
        // {
        //     path: '*',
        //     redirect: localStorage.getItem('admin-auth') ? {name: 'home'} : {name: 'login'},
        // },
    ]
});

export default router;
