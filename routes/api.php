<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {

        Route::get('{user}', 'CRUD\RetrieveController');

        Route::group(['prefix' => 'auth'], function () {

            Route::post('sign-up', 'AuthController@signUp');

            Route::post('sign-in', 'AuthController@signIn');

        });

        Route::group(['middleware' => 'auth:api'], function () {

            Route::group(['prefix' => 'utils'], function () {

                Route::put('avatar', 'UserController@updateAvatar');

            });

            Route::group(['prefix' => 'crud', 'namespace' => 'CRUD'], function () {

                Route::put('password', 'PasswordController');

                Route::put('/', 'UpdateController');

                Route::delete('/', 'DeleteController');

            });
        });

    });

    Route::group(['prefix' => 'post', 'namespace' => 'Post'], function () {

        Route::get('/', 'PostController@index');
        // TODO: make pretty
        Route::get('crud/{post}', 'CRUD\RetrieveController');

        Route::group(['middleware' => 'auth:api'], function () {

            Route::group(['prefix' => 'crud', 'namespace' => 'CRUD'], function () {

                Route::post('/', 'CreateController');
                Route::put('{post}', 'UpdateController');
                Route::delete('{post}', 'DeleteController');

            });

            Route::group(['prefix' => 'utils'], function () {

                Route::get('active/{post}', 'PostController@activeSwitcher');
                Route::get('commentable/{post}', 'PostController@commentableSwitcher');

            });

        });

    });

});
