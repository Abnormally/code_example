<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 5:19
 */

namespace App\Abnormally\Seed\Auth;

use App\Abnormally\Seed\BaseSeeder;
use App\Models\Common\Image;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserSeeder extends BaseSeeder
{

    /**
     * Execute seeder logic
     */
    public function run()
    {
        $user = User::create(
            [
                "first_name" => "Sudo",
                "surname"    => "Su",
                "nickname"   => "Abnormally",
                "email"      => "root@sudo.su",
                "password"   => \Hash::make("password"),
            ]
        );

        Image::createImageHandler($user, $user, "https://gagadget.com/media/post_big/robots.jpg");

        $user->assignRole(['super', 'admin']);
    }

}
