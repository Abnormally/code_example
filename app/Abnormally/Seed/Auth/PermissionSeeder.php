<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 11:35
 */

namespace App\Abnormally\Seed\Auth;

use App\Abnormally\Seed\BaseSeeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends BaseSeeder
{

    /**
     * Execute seeder logic
     */
    public function run()
    {
        $guards = ['web', 'api', ''];

        foreach ($guards as $guard) {
            Permission::create(
                [
                    'name'       => 'comments',
                    'guard_name' => $guard,
                ]
            );
        }
    }

}
