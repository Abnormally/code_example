<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 8:52
 */

namespace App\Abnormally\Seed\Auth;

use App\Abnormally\Seed\BaseSeeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends BaseSeeder
{

    /**
     * Execute seeder logic
     */
    public function run()
    {
        $guards = ['web', 'api', ''];
        $names  = ['super', 'admin', 'moderator'];

        foreach ($names as $name) {
            foreach ($guards as $guard) {
                Role::create(
                    [
                        'name'       => $name,
                        'guard_name' => $guard,
                    ]
                );
            }
        }
    }

}
