<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 5:16
 */

namespace App\Abnormally\Seed\Auth;

use App\Abnormally\Seed\BaseSeeder;
use Laravel\Passport\Client;
use Laravel\Passport\PersonalAccessClient;

class ClientSeeder extends BaseSeeder
{

    /**
     * Execute seeder logic
     */
    public function run()
    {
        PersonalAccessClient::create(
            [
                'client_id' => 1,
            ]
        );

        (new Client)->forceFill(
            [
                'user_id'                => 0,
                'name'                   => 'web',
                'secret'                 => str_random(40),
                'redirect'               => '#',
                'personal_access_client' => true,
                'password_client'        => true,
                'revoked'                => false,
            ]
        )->save();

        (new Client)->forceFill(
            [
                'user_id'                => 0,
                'name'                   => 'android',
                'secret'                 => str_random(40),
                'redirect'               => '#',
                'personal_access_client' => true,
                'password_client'        => true,
                'revoked'                => false,
            ]
        )->save();

        (new Client)->forceFill(
            [
                'user_id'                => 0,
                'name'                   => 'ios',
                'secret'                 => str_random(40),
                'redirect'               => '#',
                'personal_access_client' => true,
                'password_client'        => true,
                'revoked'                => false,
            ]
        )->save();
    }

}
