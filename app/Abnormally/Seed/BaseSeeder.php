<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 5:14
 */

namespace App\Abnormally\Seed;

use App\Abnormally\Contract\BaseSeeder as SeederContract;
use Illuminate\Database\Seeder;

/**
 * Class BaseSeeder
 * @package App\Abnormally\Seed
 */
abstract class BaseSeeder extends Seeder implements SeederContract
{

    //

}
