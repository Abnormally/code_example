<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 11:57
 */

namespace App\Abnormally\Permission;

use Spatie\Permission\Traits\HasRoles as BaseTrait;
use Spatie\Permission\Models\Role;

trait HasRoles
{

    use BaseTrait;


    /**
     * @param array|string[] $roles
     * @param string         $guard
     * @return \Illuminate\Database\Eloquent\Collection|Role[]
     */
    protected function getRoles(array $roles, string $guard = 'api')
    {
        $query = Role::query()->where('id', '=', 0);

        foreach ($roles as $role) {
            $query = $query->orWhere(
                [
                    ['name', '=', $role],
                    ['guard_name', '=', $guard],
                ]
            );
        }

        return $query->get();
    }

}
