<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 11:57
 */

namespace App\Abnormally\Permission;

use Spatie\Permission\Traits\HasPermissions as BaseTrait;
use Spatie\Permission\Models\Permission;

trait HasPermissions
{

    use BaseTrait;


    /**
     * @param array|string[] $permissions
     * @param string         $guard
     * @return \Illuminate\Database\Eloquent\Collection|Permission[]
     */
    protected function getPermissions(array $permissions, string $guard = 'api')
    {
        $query = Permission::query()->where('id', '=', 0);

        foreach ($permissions as $permission) {
            $query = $query->orWhere(
                [
                    ['name', '=', $permission],
                    ['guard_name', '=', $guard],
                ]
            );
        }

        return $query->get();
    }

}
