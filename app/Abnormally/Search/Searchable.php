<?php

namespace App\Abnormally\Search;

trait Searchable
{

    /**
     * Prepare searchable query
     *
     * @param $query
     * @return mixed
     */
    private function prepareQuery($query)
    {
        if (is_string($query)) {
            $query = trim($query);
            $query = explode(' ', $query);
        } elseif (is_object($query)) {
            try {
                /** @var object $query */
                $query = explode(' ', $query->toString());
            } catch (\Exception $e) {
                return null;
            }
        } elseif (!is_array($query)) {
            $query = ["$query"];
        }

        if (empty($query)) {
            return null;
        }

        return array_reduce($query, function ($result, $chunk) {
            return $result .= "%{$chunk}%";
        });
    }


    /**
     * @return array
     */
    protected function getSearchIndexes()
    {
        return isset($this->search_indexes) && is_array($this->search_indexes)
            ? $this->search_indexes
            : [];
    }


    /**
     * @param $query
     * @return $this|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder
     */
    public function search($query)
    {
        /** @var \Illuminate\Database\Eloquent\Model $model */
        $model = $this;

        if (!$query || empty($this->getSearchIndexes())) {
            return $this;
        }

        $query = $this->prepareQuery($query);

        if (!$query) {
            return $model;
        }

        foreach ($this->getSearchIndexes() as $index) {
            $model = $model->orWhere($index, 'like', $query);
        }

        return $model;
    }

}
