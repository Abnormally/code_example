<?php

namespace App\Abnormally\Auth;

use Illuminate\Container\Container;
use Laravel\Passport\HasApiTokens;

/**
 * Trait UserTokens
 *
 * @package Abnormally\Auth\Traits
 */
trait WebTokens
{

    use HasApiTokens;


    /**
     * Create a new personal access token for the user.
     *
     * @param  string $name
     * @param  array  $scopes
     * @return \Laravel\Passport\PersonalAccessTokenResult
     */
    public function createToken($name, array $scopes = [])
    {
        return Container::getInstance()
                        ->make(
                            WebClient::class
                        )
                        ->make(
                            $this->getKey(), $name, $scopes
                        );
    }

}
