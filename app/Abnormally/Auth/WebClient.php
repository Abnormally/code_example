<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 15.07.2018
 * Time: 18:33
 */

namespace App\Abnormally\Auth;

use Laravel\Passport\Client;
use Laravel\Passport\PersonalAccessTokenFactory;
use Zend\Diactoros\ServerRequest;

/**
 * Class UserClientFactory
 *
 * @package Abnormally\Auth\Factories
 */
class WebClient extends PersonalAccessTokenFactory
{

    /**
     * Slug of current user model.
     *
     * @var string
     */
    protected $name = 'web';


    /**
     * Create a request instance for the given client.
     *
     * @param  \Laravel\Passport\Client $client
     * @param  mixed                    $userId
     * @param  array                    $scopes
     * @return \Zend\Diactoros\ServerRequest
     */
    protected function createRequest($client, $userId, array $scopes)
    {
        $userClient = Client::whereName($this->name)->first();

        return (new ServerRequest)->withParsedBody(
            [
                'grant_type'    => 'personal_access',
                'client_id'     => $userClient->id,
                'client_secret' => $userClient->secret,
                'user_id'       => $userId,
                'scope'         => implode(' ', $scopes),
            ]
        );
    }

}
