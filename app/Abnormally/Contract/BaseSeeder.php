<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 5:12
 */

namespace App\Abnormally\Contract;

interface BaseSeeder
{

    /**
     * Execute seeder logic
     */
    public function run();

}
