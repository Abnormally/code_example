<?php

namespace App\Models;

use App\Abnormally\Auth\WebTokens;
use App\Abnormally\Permission\HasRoles;
use App\Models\Common\Image;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    // Actually has roles not using in this test work, but...
    use WebTokens, HasRoles, SoftDeletes;

    protected $guard_name = "api";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'surname',
        'nickname',
        'email',
        'password',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_admin',
        'is_super',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = null;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'author_id', 'id');
    }


    /**
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastComments($limit = 5)
    {
        return $this
            ->hasMany('App\Models\Common\Comment', 'author_id', 'id')
            ->orderByDesc('created_at')
            ->limit($limit);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function avatar()
    {
        return $this->morphOne('App\Models\Common\Image', 'imageble');
    }


    /**
     * @param array|\Illuminate\Support\Collection $data
     * @param string|null                          $image_url
     * @return \App\Models\User
     */
    public function createUser($data, string $image_url = null)
    {
        /** @var \App\Models\User $user */
        $user            = self::make($data);
        $user->is_active = true;
        $user->save();

        if ($image_url) {
            Image::createImageHandler($user, $user, $image_url);
        }

        return $user;
    }


    /**
     * Determines is user admin or not
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->hasAnyRole(['super', 'admin']);
    }


    /**
     * Determines is user super or not
     *
     * @return bool
     */
    public function getIsSuperAttribute()
    {
        return $this->hasRole('super');
    }

}
