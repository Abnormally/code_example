<?php

namespace App\Models;

use App\Abnormally\Permission\HasPermissions;
use App\Abnormally\Search\Searchable;
use App\Models\Common\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{

    use HasPermissions, SoftDeletes, Searchable;

    protected $guard_name = "api";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
    ];

    /**
     * Where we gonna looking for
     *
     * @var array
     */
    protected $search_indexes = [
        'title',
        'content',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_commentable',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    public function hasRole()
    {
        return false;
    }


    /**
     * Set post to commentable state or not.
     *
     * @param bool $available
     */
    public function commentsAvailable(bool $available = true)
    {
        if ($available) {
            $this->givePermissionTo('comments');
        } else {
            $this->permissions()->detach();
        }
    }


    /**
     * Can users comment this post?
     *
     * @return bool
     */
    public function getIsCommentableAttribute()
    {
        return $this->hasPermissionTo('comments');
    }


    /**
     * Post image. If it exists.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne('App\Models\Common\Image', 'imageble');
    }


    /**
     * Post author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this
            ->hasOne('App\Models\User', 'id', 'author_id')
            ->select(['id', 'nickname']);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this
            ->morphMany('App\Models\Common\Comment', 'commentable')
            ->orderByDesc('created_at');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function allComments()
    {
        return $this
            ->morphMany('App\Models\Common\Comment', 'commentable')
            ->orderByDesc('created_at');
    }


    /**
     * Just create post function.
     *
     * @param \App\Models\User                      $user
     * @param  array|\Illuminate\Support\Collection $data
     * @param string|null                           $image_url
     * @return \App\Models\Post
     */
    public static function createPost(User $user, $data, string $image_url = null)
    {
        /** @var Post $post */
        $post            = self::make($data);
        $post->author_id = $user->id;
        $post->save();

        $post->commentsAvailable();

        if ($image_url) {
            Image::createImageHandler($user, $post, $image_url);
            $post->image;
        }

        return $post;
    }

}
