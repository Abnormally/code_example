<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 3:03
 */

namespace App\Models\Common;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imageble_id',
        'imageble_type',
        'owner_id',
        'url',
        'label',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'imageble_type',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * Morph to some model, that you want connect with images.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageble()
    {
        return $this->morphTo();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function owner()
    {
        return $this->hasOne('App\Models\User', 'id', 'owner_id');
    }


    /**
     * Creates image handler.
     *
     * @param \App\Models\User                    $user
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string                              $image_url
     * @param string|null                         $label
     * @return \App\Models\Common\Image
     */
    public static function createImageHandler(User $user, Model $model, string $image_url, string $label = null)
    {
        return self::create(
            [
                'imageble_id'   => $model->id,
                'imageble_type' => get_class($model),
                'owner_id'      => $user->id,
                'url'           => $image_url,
                'label'         => $label,
            ]
        );
    }

}
