<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 3:03
 */

namespace App\Models\Common;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id',
        'title',
        'content',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];


    /**
     * Post image. If it exists.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne('App\Models\Common\Image', 'imageble');
    }


    /**
     * Post author.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this
            ->hasOne('App\Models\User', 'id', 'author_id')
            ->select(['id', 'first_name', 'surname', 'nickname', 'is_active'])
            ->with('avatar');
    }


    /**
     * Morph to some model, that you want connect with comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        $obj = $this->morphTo();

        if (request()->user() && request()->user()->is_admin) {
            $obj = $obj->withTrashed();
        }

        return $obj;
    }


    /**
     * Just create post function.
     *
     * @param \App\Models\User                      $user
     * @param  array|\Illuminate\Support\Collection $data
     * @param string|null                           $image_url
     * @return \App\Models\Common\Comment
     */
    public static function createComment(User $user, $data, string $image_url = null)
    {
        /** @var Comment $comment */
        $comment            = self::create($data);
        $comment->author_id = $user->id;
        $comment->save();

        if ($image_url) {
            Image::createImageHandler($user, $comment, $image_url);
            $comment->image;
        }

        return $comment;
    }

}
