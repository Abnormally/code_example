<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:16
 */

namespace App\Http\Requests;

interface RuledRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array;

}
