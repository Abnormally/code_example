<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 17.09.2018
 * Time: 0:11
 */

namespace App\Http\Requests\User\Utils;

use App\Http\Requests\BaseRequest;

/**
 * Class UpdateAvatarRequest
 * @package App\Http\Requests\User\Utils
 */
class UpdateAvatarRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'image_url' => 'bail|required|min:7|url'
        ];
    }

}
