<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:19
 */

namespace App\Http\Requests\User\CRUD;


use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'bail|sometimes|string|min:2|max:190',
            'surname'    => 'bail|sometimes|string|min:2|max:190',
            'nickname'   => 'bail|sometimes|string|min:2|max:190',
            'email'      => 'bail|sometimes|email|unique:users,email,' . request()->user()->id,
        ];
    }

}
