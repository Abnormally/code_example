<?php

namespace App\Http\Requests\User\CRUD;

use App\Http\Requests\BaseRequest;

class DeleteRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !request()->user()->is_super;
    }


    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

}
