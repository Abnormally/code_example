<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 6:30
 */

namespace App\Http\Requests\User\CRUD;


use App\Http\Requests\BaseRequest;

class PasswordRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'old_password' => 'bail|required|string|min:4',
            'password'     => 'bail|required|string|min:4|confirmed',
        ];
    }

}
