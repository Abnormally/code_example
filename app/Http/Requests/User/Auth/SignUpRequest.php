<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:18
 */

namespace App\Http\Requests\User\Auth;

use App\Http\Requests\BaseRequest;

class SignUpRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'bail|sometimes|string|min:2|max:190',
            'surname'    => 'bail|sometimes|string|min:2|max:190',
            'avatar'     => 'bail|sometimes|string|min:7|url',
            'nickname'   => 'bail|required|string|min:2|max:190',
            'email'      => 'bail|required|email|max:190|unique:users,email',
            'password'   => 'bail|required|string|min:4|confirmed',
        ];
    }

}
