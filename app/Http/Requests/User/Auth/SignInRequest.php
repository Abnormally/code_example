<?php

namespace App\Http\Requests\User\Auth;

use App\Http\Requests\BaseRequest;

class SignInRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => 'bail|required|email|exists:users,email',
            'password' => 'bail|required|string|min:4',
        ];
    }

}
