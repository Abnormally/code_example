<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 17.09.2018
 * Time: 16:06
 */

namespace App\Http\Requests\Post;

use App\Http\Requests\BaseRequest;

abstract class BaseAuthorRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        /** @var \App\Models\User $user */
        $user = $this->user();

        /** @var \App\Models\Post $post */
        $post = \Route::current()->parameter('post');

        return $post && ($user->id == $post->author_id || $user->is_admin);
    }


    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

}
