<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:54
 */

namespace App\Http\Requests\Post\CRUD;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest
{

    /**
     * Get rules keys for fill model only with defined data (for example)
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'     => 'bail|required|string|min:4|max:190',
            'content'   => 'bail|required|string|min:4|max:2000',
            'image_url' => 'bail|sometimes|string|url'
        ];
    }

}
