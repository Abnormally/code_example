<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:40
 */

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\Utils\ActivateRequest;
use App\Http\Requests\Post\Utils\CommentableRequest;
use App\Models\Post;

class PostController extends Controller
{

    /** @var \App\Models\User user */
    protected $user = null;


    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->user = request()->user();
    }


    /**
     * Main posts page
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $posts = (new Post);

        if (request()->query('query')) {
            $posts = $posts->search(request()->query('query'));
        }

        if ($this->user && $this->user->is_admin) {
            $posts = $posts->withTrashed();
        }

        return response()->json(
            [
                'posts' => $posts
                    ->where('is_active', 1)
                    ->with(['image', 'author'])
                    ->orderByDesc('created_at')
                    ->paginate(request()->query('limit') ?: 8, ['*']),
            ]
        );
    }


    /**
     * @param \App\Models\Post                              $post
     * @param \App\Http\Requests\Post\Utils\ActivateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activeSwitcher(Post $post, ActivateRequest $request)
    {
        if ($this->user->id !== $post->author_id) {
            return response()->json(
                [
                    "success" => false,
                    "message" => "Not author"
                ], 403
            );
        }

        $post->is_active = !$post->is_active;
        $post->update();
        $post->image;

        return response()->json(
            [
                "success" => true,
                "message" => "Converted",
                "post"    => $post,
            ]
        );
    }


    /**
     * @param \App\Models\Post                                 $post
     * @param \App\Http\Requests\Post\Utils\CommentableRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentableSwitcher(Post $post, CommentableRequest $request)
    {
        if ($this->user->id !== $post->author_id) {
            return response()->json(
                [
                    "success" => false,
                    "message" => "Not author"
                ], 403
            );
        }

        if ($post->is_commentable) {
            $post->commentsAvailable(false);
        } else {
            $post->commentsAvailable();
        }

        $post->image;

        return response()->json(
            [
                "success" => true,
                "message" => "Converted",
                "post"    => $post,
            ]
        );
    }

}
