<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:37
 */

namespace App\Http\Controllers\Post\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CRUD\CreateRequest;
use App\Models\Post;

class CreateController extends Controller
{

    /**
     * @param \App\Http\Requests\Post\CRUD\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CreateRequest $request)
    {
        return response()->json(
            [
                'success' => true,
                'post'    => Post::createPost(
                    $request->user(),
                    $request->only(
                        [
                            'title',
                            'content',
                        ]
                    ),
                    $request->get('image_url')
                )
            ], 201
        );
    }

}
