<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:37
 */

namespace App\Http\Controllers\Post\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CRUD\UpdateRequest;
use App\Models\Common\Image;
use App\Models\Post;

class UpdateController extends Controller
{

    /**
     * @param \App\Models\Post                           $post
     * @param \App\Http\Requests\Post\CRUD\UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Post $post, UpdateRequest $request)
    {
        if ($request->has('image_url')) {
            Image::createImageHandler($request->user(), $post, request('image_url'));
            $post->image;
        }

        return response()->json(
            [
                'success' => $post->update($request->only(
                    [
                        'title',
                        'content',
                    ]
                )),
                'post'    => $post,
            ]
        );
    }

}
