<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:36
 */

namespace App\Http\Controllers\Post\CRUD;

use App\Http\Controllers\Controller;
use App\Models\Post;

class RetrieveController extends Controller
{

    /**
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Post $post)
    {
        $post->author;

        if (($requester = request()->user('api')) && $requester->is_admin) {
            $post->allComments;
        } else {
            $post->comments;
        }

        $post->image;

        return response()->json($post);
    }

}
