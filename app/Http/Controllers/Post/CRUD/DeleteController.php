<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:36
 */

namespace App\Http\Controllers\Post\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CRUD\DeleteRequest;
use App\Models\Post;

class DeleteController extends Controller
{

    /**
     * @param \App\Models\Post                           $post
     * @param \App\Http\Requests\Post\CRUD\DeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __invoke(Post $post, DeleteRequest $request)
    {
        return response()->json(
            [
                'success' => $post->delete(),
            ]
        );
    }

}
