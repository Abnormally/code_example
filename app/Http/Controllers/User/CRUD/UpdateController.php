<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:04
 */

namespace App\Http\Controllers\User\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CRUD\UpdateRequest;

class UpdateController extends Controller
{

    /**
     * @param \App\Http\Requests\User\CRUD\UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(UpdateRequest $request)
    {
        /** @var \App\Models\User $user */
        $user = $request->user();

        $user->avatar;

        return response()->json(
            [
                'success' => $user->update(array_filter($request->only(
                    [
                        'first_name',
                        'surname',
                        'nickname',
                        'email',
                    ]
                ))),
                'user'    => $user->only(
                    [
                        'id',
                        'first_name',
                        'surname',
                        'nickname',
                        'email',
                        'avatar',
                        'is_admin',
                        'is_super',
                    ]
                ),
            ]
        );
    }

}
