<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:03
 */

namespace App\Http\Controllers\User\CRUD;

use App\Http\Controllers\Controller;
use App\Models\User;

/**
 * Class RetrieveController
 * @package App\Http\Controllers\User\CRUD
 */
class RetrieveController extends Controller
{

    /**
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(User $user)
    {
        if (request()->user() && request()->user()->is_admin) {
            // Add last 5 user comments if requester is admin
            $user->lastComments;
        }

        $user->avatar;

        return response()->json($user->only(
            [

                'id',
                'first_name',
                'surname',
                'nickname',
                'avatar',
                'is_admin',
                'is_super',
                'lastComments',
            ]
        ));
    }

}
