<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:04
 */

namespace App\Http\Controllers\User\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CRUD\DeleteRequest;

class DeleteController extends Controller
{

    /**
     * @param \App\Http\Requests\User\CRUD\DeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __invoke(DeleteRequest $request)
    {
        /** @var \App\Models\User $user */
        $user = $request->user();

        if (!\Hash::check(request('password'), $user->password) || $user->is_admin) {
            return response()->json(
                [
                    'message' => "Not allowed",
                ], 403
            );
        }

        return response()->json(
            [
                'success' => $user->delete(),
            ]
        );
    }

}
