<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 3:24
 */

namespace App\Http\Controllers\User\CRUD;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CRUD\PasswordRequest;

/**
 * Class PasswordController
 * @package App\Http\Controllers\User\CRUD
 */
class PasswordController extends Controller
{

    /**
     * @param \App\Http\Requests\User\CRUD\PasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(PasswordRequest $request)
    {
        /** @var \App\Models\User $user */
        $user = $request->user();

        if (!\Hash::check(request('old_password'), $user->password)) {
            return response()->json(
                [
                    "message" => "Passwords not match"
                ], 403
            );
        }

        $user->password = \Hash::make(request('password'));
		$user->update();

        return response()->json(
            [
                "success" => true,
                // __("some.translate.string") // TODO: future
                "message" => "Password updated",
            ]
        );
    }

}
