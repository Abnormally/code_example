<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 14.09.2018
 * Time: 4:40
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Utils\UpdateAvatarRequest;
use App\Models\Common\Image;

/**
 * Class UserController
 * @package App\Http\Controllers\User
 */
class UserController extends Controller
{

    /**
     * @param \App\Http\Requests\User\Utils\UpdateAvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAvatar(UpdateAvatarRequest $request)
    {
        /** @var \App\Models\User $user */
        $user = $request->user();

        $avatar = $user->avatar;

        if (!$avatar) {
            $avatar = Image::createImageHandler($user, $user, trim(request('image_url')));
        } else {
            $avatar->url = trim(request('image_url'));
            $avatar->update();
        }

        $user->avatar;

        return response()->json(
            [
                'success' => $avatar->update(),
                'message' => 'All seems is ok',
                'user'    => $user,
            ]
        );
    }

}
