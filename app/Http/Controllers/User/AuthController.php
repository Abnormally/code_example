<?php
/**
 * Created by PhpStorm.
 * User: Abnormally
 * Date: 16.09.2018
 * Time: 8:01
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\SignInRequest;
use App\Http\Requests\User\Auth\SignUpRequest;
use App\Models\Common\Image;
use App\Models\User;

class AuthController extends Controller
{

    /**
     * @param \App\Http\Requests\User\Auth\SignInRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(SignInRequest $request)
    {
        $user = User::whereEmail($request->get('email'))->first();

        if (!\Hash::check(request('password'), $user->password)) {
            return response()->json(
                [
                    "success" => false,
                    "message" => "Data is invalid",
                ], 401
            );
        }

        return $this->withToken($user);
    }


    /**
     * @param \App\Http\Requests\User\Auth\SignUpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUp(SignUpRequest $request)
    {
        /** @var User $user */
        $user           = User::make($request->all());
        $user->password = \Hash::make($request->get('password'));
        $user->save();

        if ($request->has('avatar')) {
            Image::createImageHandler($user, $user, $request->get('avatar'));
        }

        return $this->withToken($user, true);
    }


    /**
     * @param \App\Models\User $user
     * @param bool             $created
     * @return \Illuminate\Http\JsonResponse
     */
    private function withToken(User $user, $created = false)
    {
        $user->avatar;

        return response()->json(
            [
                'success' => true,
                'message' => 'Authorized successfully',
                'user'    => $user->only(
                    [
                        'id',
                        'first_name',
                        'surname',
                        'nickname',
                        'email',
                        'avatar',
                        'is_admin',
                        'is_super',
                    ]
                ),
                'auth'    => [
                    'token_type'   => 'Bearer',
                    'access_token' => $user->createToken('web')->accessToken,
                ],
            ], $created ? 201 : 200
        );
    }

}
